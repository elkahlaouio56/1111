import { Routes, Route} from 'react-router-dom'
import Home from '../component/mainpages/Home'
import Shop from '../component/Shop/Shop'
import Cart from '../component/mainpages/Cart'
import ProductDetails from '../component/mainpages/ProductDetails'
import Checkout from '../component/mainpages/Checkout'
import Login from '../component/mainpages/Login'
import Signup from '../component/mainpages/Signup'


const Routers = () => {
    return <Routes>
        <Route path='home' element={<Home/>}/>
        <Route path='Shop' element={<Shop/>}/>
        <Route path='Shop/:id' element={<ProductDetails/>}/>
        <Route path='cart' element={<Cart/>}/>
        <Route path='checkout' element={<Checkout/>}/>
        <Route path='login' element={<Login/>}/>
        <Route path='signup' element={<Signup/>}/>



    </Routes>
};
export default Routers 
