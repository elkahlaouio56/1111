import React, { useState } from 'react';
// import ProductContainer from '../components/ProductContainer';
import {AiFillStar} from 'react-icons/ai' ;
import {HiOutlineShoppingBag} from 'react-icons/hi';
import { Link } from 'react-router-dom';
// import TabPanel from './TabPanel' ;
import MyGallery from './GalleryImages';
import {FaBlenderPhone} from 'react-icons/fa' ;

import {FaShippingFast} from 'react-icons/fa' ;
import {BiTimer } from 'react-icons/bi' ;
import {FcCheckmark} from 'react-icons/fc';
import {CgCornerLeftDown} from 'react-icons/cg';
import {FcShipped} from 'react-icons/fc';




export default function SingleProduct() {


    const[click , setClick]= useState() ;


    const handleClick = () =>{

      setClick(!click);
    }

    return(
        <>

       <div className='container'>
           <div className="product-page">
               <section className='product-page-img'>
                    <MyGallery />
               </section>

               <section className="product-page-infos animate__animated animate__fadeIn">
                   <h3>Pomme golden moyen calibre 1Kg - FILIERE M</h3>
                   <h6 style={{'display':'flex','flexDirection':'column'}}><span><AiFillStar /><AiFillStar /><AiFillStar /><AiFillStar /><AiFillStar />  </span>  (18 reviews)</h6>
                   <h2>33 MAD</h2>
                   <span>Les frais de livraison sont inclus  <FcShipped /></span>
                   <p>
                   Pomme golden 24-26 vendue et préparée au KG.
                    La FILIERE MARJANE est cultivée localement par nos partenaires experts agronomes assurant une origine, qualité, fraîcheur et saveur de produit exceptionnelle.
                    Contenance : 1 Kg.

                    * Images non contractuelles.
                    ** Sélection de la meilleure qualité disponible.
                    *** Légères variations de +/-5% du poids préparé.
                    **** Préparation au respect  strict de l'hygiène. Emballage en atelier Marjane.
                   </p>
                   <section style={{'display':'flex','alignItems':'flex-start','justifyContent':'space-between'}}><span style={{'fontSize':'12px','border':'1px solid #ffffff8f','border-radius':'5px','padding':'1px 5px'}}><FcCheckmark /> En Stock</span>  <button  onClick={handleClick} style={{'display':'flex','flexDirection':'column','border':'1px solid #ffffff91','borderRadius':'5px','padding':'2px 10px','background':'transparent','margin-bottom':'2px'}}><span >{click ? <CgCornerLeftDown className='animate__animated animate__fadeInUp'/> : '+'} livraison <FaShippingFast /></span></button></section>
                   <section className='animate__animated animate__fadeInDown' style={{display: click ? 'flex' : 'none' ,'flexDirection':'column','border':'1px solid #ffffff91','borderRadius':'5px','padding':'5px 14px','marginBottom':'5px'}}>
                   <span className='animate__animated animate__fadeInUp' style={{'fontSize':'12px'}}> - Livraison Gratuit au <strong>Maroc </strong>- 48h <BiTimer /></span>
                   <span className='animate__animated animate__fadeInUp' style={{'fontSize':'12px'}}> - Livraison à 95€ <strong> Europ - Pays arabes</strong> : via ARAMEX ~ 5j <BiTimer /></span>
                   <span className='animate__animated animate__fadeInUp' style={{'fontSize':'12px'}}> - Livraison à 110€ <strong> Canada - Etats-unis</strong> : via ARAMEX ~ 5j <BiTimer /></span>
                   </section>
                   <p><a className='link-call' href="tel:+212 536 68 67 45">Commander par Telephone <FaBlenderPhone /></a></p>
                   <Link to="/formulaire" >ACHETER MAINTENANT <span style={{'fontSize':'18px'}}><HiOutlineShoppingBag /></span></Link>
               </section>
           </div>
           <div className="product-page-desc">
           {/* <TabPanel /> */}
           
           </div>

          


           </div>      
        </>
    );
}