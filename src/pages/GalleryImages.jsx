import { Component } from 'react';
import ImageGallery from 'react-image-gallery';


const images = [
    {
        original: '/images/top/category-1.png',
        thumbnail: '/images/top/category-1.png',
        },
  {
    original: '/images/top/category-2.png',
        thumbnail: '/images/top/category-2.png',
  },
  {
    original: '/images/top/category-3.png',
    thumbnail: '/images/top/category-3.png',
  }
  
];

export default class MyGallery extends Component {
  render() {
    return <ImageGallery items={images} />;

  }
}