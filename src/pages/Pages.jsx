import React from 'react'
import FlashDeals from '../component/flashDeals/FlashDeals'
import Home from  "../component/mainpages/Home"
import Offre from '../component/offres/Detailles'




const Pages = ({ productItems, addToCart, CartItem }) => {
    return (
        <>
             <Home CartItem={CartItem} />
             <FlashDeals productItems={productItems} addToCart={addToCart} />
            
           <Offre/>
          </>
    )
}
export default Pages