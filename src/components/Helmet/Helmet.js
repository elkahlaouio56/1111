 
 import React from 'react'

 const Helmet = (props) =>{
    document.title = '11-11Fresh -'+props.title
    return (
            <div className='w-100' >{props.children}</div>
    )
 }
 export default Helmet