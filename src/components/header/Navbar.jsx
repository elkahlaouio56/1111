
import { NavLink } from "react-router-dom"
import { Container , Row } from "reactstrap";
const nav__links =[
  {
    path:'',
    display:'Accueil'
  },
  {
    path:'Shop',
    display:'Categories'
  },
  {
    path:'Cart',
    display:'Cart'
  },
  {
    path:'Contact',
    display:'Contact'
  },
  
  ]
  
const Navbar = () => {
  // Toogle Menu
  return (
    <>
      <header className='header'>
       <Container >
        <Row>
      <div className='navigation'>
            <ul className="menu">
              {
                nav__links.map((item,index) => (
                  <li className="nav__item" key={index}>
                    <NavLink to={item.path} className={(navClass)=>navClass.isActive ?'nav__active' :''}>{item.display}
                    </NavLink>
                  </li>
                
                ))
              }
            </ul>


          </div>
        </Row>
       </Container>
         
        
      </header>
    </>
  )
}

export default Navbar


