import React from "react"
import { Link } from "react-router-dom"
import app from "../../assets/images/app.png"
import app2 from "../../assets/images/app2.png"
import logo from "../../assets/images/logo.png"
import "./style.css"
import {BsLinkedin,BsInstagram,BsFacebook,BsTwitter} from "react-icons/bs";
import ScrollToTop from "react-scroll-to-top";
const Footer = () => {
  return (
    <>
    
    <section>
    <div class="p-3  bg-dark " >
    <ScrollToTop smooth top="300" color="#00abe9"/>
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-8 offset-xl-2">
                                <div class="row align-items-center">
                                    <div class="col-lg-5 cta-txt">
                                        <h3 class="cta-title ">Les Nouvelles</h3>
                                        <p class="cta-desc ">Abonnez-vous pour obtenir des informations sur les produits et les coupons.</p>
                                        </div>
                                        <div class="col-lg-7">
                                            <form action="#">
                                                <div class="input-group">
                                                    <input type="email" class="form-control" placeholder="Entrez votre adresse email" aria-label="Email Adress" required=""/>


                                                        <div class="input-group-append">
                                                        <button class="btn" type="submit">Abonnez</button>
                                                        </div>
                                             </div>
                                             </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
      </div>
    </section>
      <footer className="py-3">
        <div className="container-xl">
            <div className="row">
                <div className="col-3">
                <img src={logo} class="footer-logo" alt="Footer Logo" />
                    
                    <div className="social_icons c-flex align-items-center gap-30 mt-2">
                        <a className="link" href="">
                            <BsLinkedin className=" fs-5"/>
                        </a>
                        <a  className="link" href="">
                        <BsInstagram/>
                        </a>
                        <a className="link" href="">
                           <BsFacebook />
                        </a>
                        <a className="link" href="">
                           <BsTwitter/>
                        </a>

                    </div>

                </div>
                <div className="col-3">
                    <h3 className="mb-4">À propos</h3>
                    <div className=" footer-link d-flex flex-column" >
                    
                         <a className=" footer__link" href=""> Privacy Policy</a>
                         <a className="footer__link" href=""> About</a>
                         <a className="footer__link" href=""> Termes & Conditions</a>
                         <a className="footer__link" href=""> Blogs</a>
                       
                    
                    </div>
                </div>
                <div className="col-3">
                    <h3 className="mb-4">Besoin d'aide?</h3>
                        <div className="footer-link d-flex flex-column" >
                        <a className="footer__link" href="">Nous Contacter</a>
                        <a className="footer__link" href=""> Faq</a> 
                        
                        </div>
                </div>
                <div className="col-3">
                <h3 className="mb-4">Notre Application</h3>

                          <p>Avec l'application 11&11, vous pouvez faire vos achats encore plus rapidement</p>
                          <ul class=" flex-row"> 
                           <li class="img-small">
                                    <a target="_blank" rel="noreferrer" href="" class="app">
                                        <img src={app} alt="2 pictos apple et google play" class="app"/>
                                   </a>
                                </li>
                            <li class="img-small">
                                <a target="_blank" rel="noreferrer" href="" class="app">
                                <img src={app2} alt="2 pictos apple et google play" class="app"/>
                                  
                                    </a>
                                    </li>
                                    </ul>
                </div>
            </div>
         </div>
     


            <div className="container-xxl">
                <div className="row">
                    <div className="col-12">
                        <p className="text-center mb-0 text-blue">
                            &copy;{new Date().getFullYear()}: Powered by Developper's
                        </p>
                    </div>
                </div>

            </div>
      </footer>
    
    </>
)
}

export default Footer
