import React,{useState} from "react"
import { Redirect } from "react-router-dom";
import Swal from 'sweetalert2'
import Home from "./Home";
import "./login.css"

const Login = () => {
  const [emaillog, setEmaillog] = useState(" ");
  const [passwordlog, setPasswordlog] = useState(" ");

  const [flag, setFlag] = useState(false);


  
  function handleLogin(e) {
    e.preventDefault();
    let pass = localStorage
      .getItem("Password")
      .replace(/"/g, "");
    let mail = localStorage.getItem("Email").replace(/"/g, "");
    

    if (!emaillog || !passwordlog) {
      Swal.fire({  
        text:'please enter your email and password',
        icon: 'error'
      })
     
    } else if (passwordlog !== pass || emaillog !== mail) {
      Swal.fire({
        
        text:'email or password not correct',
        icon: 'error'
      })
    } else {
       localStorage.setItem("connect" , true)
       window.location.href = "/home"
      
    }
  }
  return (
    <>

      <section className='login'>
        <div className='container'>
          {/* <div className='backImg'>
            <img src={back} alt='' />
            <div className='text'>
              <h3>Login</h3>
              <h1>My ACcount</h1>
            </div>
          </div> */}

          <form onSubmit={handleLogin}>
            <span>Username or Email address *</span>
            <input type='text' required onChange={(event) => setEmaillog(event.target.value)}/>
            <span>Password * </span>
            <input type='password' required onChange={(event) => setPasswordlog(event.target.value)}/>
            <button className='button'>Log in </button>
         
          </form>
         
        </div>
      </section>
   
                    
    </>
  )
}
export default Login
