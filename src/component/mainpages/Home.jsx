import React from "react"
import "./Home.css"
import Slider from "./Slider"
import Helmet from "../../components/Helmet/Helmet"  

import TopCate from "../top/TopCate"


const Home = () => {
  return (
    <Helmet title={"Home"}>

      <section className='home'>
        <div className='container d_flex'>
          
          <Slider />
               
        </div>
      </section>
      <TopCate/>

    </Helmet>    
    
  )
}

export default Home
