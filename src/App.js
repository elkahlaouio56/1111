import React, { useState } from "react"
import "./App.css"
import Header from "./components/header/Header";
import { BrowserRouter as Router, Switch , Route } from "react-router-dom"
import Pages from "./pages/Pages"
import Cart from "./component/mainpages/Cart"
import Data from "./component/Data"
import Footer from "./components/footer/Footer";
import Shop from "./component/mainpages/Shop"
import Login from "./component/mainpages/Login";
import SingleProduct from "./pages/SingleProduct";


function App() {
   
 

  //Step 1 :
  const { productItems } = Data


  //Step 2 :
  const [CartItem, setCartItem] = useState([])

  //Step 4 :
  const addToCart = (product) => {
    
    const productExit = CartItem.find((item) => item.id === product.id)
    
    if (productExit) {
      setCartItem(CartItem.map((item) => (item.id === product.id ? { ...productExit, qty: productExit.qty + 1 } : item)))
    } else {
      
      setCartItem([...CartItem, { ...product, qty: 1 }])
    }
  }
  return (
  
       <>
      <Router>
        <Header CartItem={CartItem} />
        <Switch>
          <Route path='/'  exact>
            <Pages productItems={productItems} addToCart={addToCart} />
          </Route>
          <Route path='/SingleProduct'  exact>
            <SingleProduct />
          </Route>
          <Route path='/cart' exact>
            <Cart CartItem={CartItem} addToCart={addToCart} />
          </Route>
          <Route path='/login' exact>
            <Login />
          </Route>
          <Route path='/Shop' exact>
            <Shop />
          </Route>
        </Switch>
        <Footer />
      </Router>
   

    </>
  )
}


export default App;
